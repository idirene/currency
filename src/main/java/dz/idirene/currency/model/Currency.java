package dz.idirene.currency.model;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table; 
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDate;
import javax.persistence.Id; 
import javax.validation.constraints.Size;

/**
 * Created by TOSHIBA on 21/12/2018.
 */
@Entity
@Data
@XmlRootElement
@AllArgsConstructor
@NoArgsConstructor
@Table(schema = "sch_currency", name = "mc40200")
@ToString(of = "CURNCYID", doNotUseGetters = true)
@EqualsAndHashCode(of = "CURNCYID", doNotUseGetters = true)
public class Currency {

    @Id
    @Size(min = 0, max = 15)
    private String CURNCYID;

    private Integer CURRNIDX;

    @Size(min = 0, max = 31)
    private String CRNCYDSC;

    @Size(min = 0, max = 61)
    private String CRNCYDSCA;

    @Size(min = 0, max = 3)
    private String CRNCYSYM;

    private boolean INCLSPAC;

    @Column(columnDefinition = "SMALLINT")
    @Type(type = "org.hibernate.type.ShortType")
    private short NEGSYMBL;
    
    @Column(columnDefinition = "SMALLINT")
    @Type(type = "org.hibernate.type.ShortType")
    private short NGSMAMPC;
    
    @Column(columnDefinition = "SMALLINT")
    @Type(type = "org.hibernate.type.ShortType")
    private short DECSYMBL;
    
    @Column(columnDefinition = "SMALLINT")
    @Type(type = "org.hibernate.type.ShortType")
    private short THOUSSYM;

    @Size(min = 0, max = 25)
    private String CURTEXT_1;

    @Size(min = 0, max = 25)
    private String CURTEXT_2;

    @Size(min = 0, max = 25)
    private String CURTEXT_3;

    @Size(min = 0, max = 55)
    private String CURTEXTA_1;

    @Size(min = 0, max = 55)
    private String CURTEXTA_2;

    @Size(min = 0, max = 55)
    private String CURTEXTA_3;

    private LocalDate DEX_ROW_TS;

    private Integer DEX_ROW_ID;

}
