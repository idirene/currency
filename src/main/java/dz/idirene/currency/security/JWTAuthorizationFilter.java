/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dz.idirene.currency.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;


public class JWTAuthorizationFilter extends OncePerRequestFilter {

    public JWTAuthorizationFilter() {
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        response.addHeader("Access-Control-Allow-Origin", "*");
        //entetes a authorizé
        response.addHeader("Access-Control-Allow-Headers",
                "Origin, "
                + "Accept, "
                + "X-Requested-With,"
                + "Content-Type,"
                + "Access-Control-Request-Method, "
                + "Access-Control-Request-Headers, "
                + "Authorization");
        //authorise a exposé ces entetes
        response.addHeader("Access-Control-Expose-Headers",
                "Access-Control-Allow-Origin, "
                + "Access-Control-Allow-Credentials, "
                + "authorization");
         response.addHeader("Access-Control-Allow-Methods","GET,POST,PUT,DELETE,PATCH");
//
        System.out.println( "cookie =====> "+request.getCookies());
        System.out.println( "cookie =====> "+request.getHeader("cookie"));
        System.out.println( "header =====> "+request.getHeader("authorization"));
        if (request.getMethod().equals("OPTIONS")) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else if (request.getRequestURI().equals("/login")) {
            System.out.println("(request.getRequestURI()>>>"+request.getRequestURL().toString());
            filterChain.doFilter(request, response);
            return;
        } else {
            String jwt = request.getHeader(SecurityParams.JWT_HEADER_NAME);
            System.out.println(" doFilterInternal jwt=" + jwt);
            if (jwt == null || !jwt.startsWith(SecurityParams.JWT_HEADER_PREFIX)) {
                filterChain.doFilter(request, response);
                return;
            }
            try {
                Algorithm algorithm = Algorithm.HMAC256(SecurityParams.SECRET);
                JWTVerifier verifier = JWT.require(algorithm)
                        //                    .withIssuer("auth0")
                        .build();
                DecodedJWT decodedJWT = verifier.verify(jwt.substring(SecurityParams.JWT_HEADER_PREFIX.length()));

                String username = decodedJWT.getSubject();

                System.out.println("doFilterInternal username=" + username);
                List<String> roles = decodedJWT.getClaims().get("roles").asList(String.class);

                Collection<GrantedAuthority> authoritys = new ArrayList<>();

                roles.forEach(r -> {
                    authoritys.add(new SimpleGrantedAuthority(r));
                });
                UsernamePasswordAuthenticationToken userToSpring
                        = new UsernamePasswordAuthenticationToken(username, null, authoritys);
                //charger l'utilisateur pour que spring le prise en charge
                SecurityContextHolder.getContext().setAuthentication(userToSpring); 
                filterChain.doFilter(request, response);
                
            } catch (JWTVerificationException jwtve) {
                throw new RuntimeException("doFilterInternal Error Token >>>" + jwtve);
            }
        }
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
