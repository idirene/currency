/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dz.idirene.currency.security;
import com.sun.security.auth.UserPrincipal;
import dz.idirene.currency.model.admin.AppUser;
import io.jsonwebtoken.*;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

//    @Autowired
    AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {

            AppUser appUser = new ObjectMapper().readValue(request.getInputStream(), AppUser.class);
            System.out.println( "attempt auth " +appUser.getUsername());
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(appUser.getUsername(), appUser.getPassword()));

        } catch (IOException ex) {
            throw new RuntimeException(JWTAuthenticationFilter.class.getName() + "<-->" + ex);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
            Authentication authResult) throws IOException, ServletException {
        System.out.println( " sucess auth ");
        User userSpring = (User) authResult.getPrincipal();

        List<String> roles = new ArrayList<>();
        authResult.getAuthorities().forEach(r -> {
            roles.add(r.getAuthority());
        });

        String jwt = JWT.create()
                .withIssuer(request.getRequestURI())
                .withSubject(userSpring.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + SecurityParams.EXPIRATION))
                .withArrayClaim("roles", roles.toArray(new String[roles.size()]))
                .sign(Algorithm.HMAC256(SecurityParams.SECRET));
        System.out.println("request.getRequestURI()>>>" + request.getRequestURI());
        response.addHeader(SecurityParams.JWT_HEADER_NAME, jwt);

//        YOUCEF COOKIE
        Cookie cookie = new Cookie("auth", jwt);
        cookie.setPath("/");
        response.addCookie(cookie);
        response.addHeader("cookie", cookie.toString());

//String jwt = generateToken(authResult, response);
//        System.out.println(  "token " +jwt );
//        FIN COOKIE
//        super.successfulAuthentication(request, response, chain, authResult); 
//To change body of generated methods, choose Tools | Templates.
    }
    public String generateToken(Authentication authentication ,HttpServletResponse response) {

//        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        User userSpring = (User) authentication.getPrincipal();

        List<String> roles = new ArrayList<>();
        authentication.getAuthorities().forEach(r -> {
            roles.add(r.getAuthority());
        });
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + SecurityParams.EXPIRATION);
        Claims claims = Jwts.claims();
        claims.setSubject(userSpring.getUsername());


        String jwt = Jwts.builder()
                .setSubject(userSpring.getUsername())
                .setIssuedAt(new Date())
                .setClaims(claims)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, SecurityParams.SECRET)
                .compact();
        response.addHeader(SecurityParams.JWT_HEADER_NAME, jwt);
        claims.put("roles",roles.toArray(new String[roles.size()]));
        Cookie cookie = new Cookie("auth", jwt);
        cookie.setPath("/");
        response.addCookie(cookie);
        response.addHeader("cookie", cookie.toString());
        return jwt;

    }

    public Long getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(SecurityParams.SECRET)
                .parseClaimsJws(token)
                .getBody();

        return Long.parseLong(claims.getSubject());
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(SecurityParams.SECRET).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty.");
        }
        return false;
    }
}
