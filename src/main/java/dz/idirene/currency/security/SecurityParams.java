/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dz.idirene.currency.security;


public interface SecurityParams {
    public static final String JWT_HEADER_NAME = "authorization";
    public static final String JWT_HEADER_PREFIX = "Bearer ";
    public static final String SECRET = "zeroual@mohamed";
    public static final long EXPIRATION = 10*24*3600*1000;
}
