/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dz.idirene.currency.repository.admin;

import java.io.Serializable;

import dz.idirene.currency.model.admin.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
//

public interface AppUserRepository extends JpaRepository<AppUser, Long>{
    public AppUser findByUsername(String username);
}
