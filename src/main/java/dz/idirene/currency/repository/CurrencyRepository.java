package dz.idirene.currency.repository;

import dz.idirene.currency.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CurrencyRepository extends JpaRepository<Currency, String> {


}
