/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dz.idirene.currency.services.admin;


import dz.idirene.currency.model.admin.AppRole;
import dz.idirene.currency.model.admin.AppUser;
import dz.idirene.currency.model.admin.UserForm;
import dz.idirene.currency.repository.admin.AppRoleRepository;
import dz.idirene.currency.repository.admin.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private AppRoleRepository appRoleRepository;
    @Autowired
    private BCryptPasswordEncoder bcpe;
    @Autowired
    private Environment env;

    @Override
    public long countUser() {
        return appUserRepository.count();
    }

    @Override
    public AppUser findUserByUsername(String username) {
    return  appUserRepository.findByUsername(username);
    }

    @Override
    public AppUser saveUser(String username, String password, String confirmedPassword) {
        //exist ou pas
        AppUser user = appUserRepository.findByUsername(username);
        if (user != null) {
            throw new RuntimeException(env.getProperty("message.userExist"));
        }
        if (!password.equals(confirmedPassword)) {
            throw new RuntimeException("Please confirm your password");
        }

        AppUser appUser = new AppUser();
        appUser.setUsername(username);
        appUser.setPassword(bcpe.encode(password));
//        appUser.setActive(true);
        appUserRepository.save(appUser);

//        addRoleToUser(username, "ADMIN");
        return appUser;
    }

    @Override
    public AppRole saveRole(AppRole role) {
        AppRole appRole = appRoleRepository.findByRoleName(role.getRoleName());
        if (appRole == null) {
            return appRoleRepository.save(role);
        } else {
            throw new RuntimeException("Role exists ");
        }
    }

    @Override
    public AppUser loadUserByUsername(String username) {
        return appUserRepository.findByUsername(username);
    }

    @Override
    public void addRoleToUser(String username, String rolename) {
        AppUser appUser = appUserRepository.findByUsername(username);
        if (appUser == null) {
            throw new RuntimeException("User dose not exists");
        }
        AppRole appRole = appRoleRepository.findByRoleName(rolename);
        if (appRole == null) {
            throw new RuntimeException("Role " + rolename + " dose not  exists");
        }
        appUser.getRoles().add(appRole);
    }


    //    @Override
    public void addRoleToUser2(String username, List<String> rolenameList) {
        List<String> rolenameNewList = new ArrayList<>();
        AppUser appUser = appUserRepository.findByUsername(username);

        if (appUser.getRoles().isEmpty()) {
            rolenameList.forEach(newRoleAddToUser -> {
                addRoleToUser(username, newRoleAddToUser);
            });
        } else {
            appUser.getRoles().forEach(userRrole -> {
                rolenameList.forEach(newRole -> {
                    if (!userRrole.getRoleName().equals(newRole)) {
                        rolenameNewList.add(newRole);
                    }
                });
            });

            rolenameNewList.forEach(newRoleAddToUser -> {
                addRoleToUser(username, newRoleAddToUser);
            });
        }

    }

    @Override
    public AppUser addUser(UserForm userForm) {
//        AppUser appUser = new AppUser(null, userForm.getUsername(), password, true, direction, roles, userTickets, helpDeskTickets)

        //exist ou pas
        AppUser user = appUserRepository.findByUsername(userForm.getUsername());
        if (user != null) {
            throw new RuntimeException(env.getProperty("message.userExist"));
        }
        if (!userForm.getPassword().equals(userForm.getConfirmedPassword())) {
            throw new RuntimeException(env.getProperty("message.confirmedPassword"));
        }
        System.out.println("addUser(UserForm userForm)");
        System.out.println(userForm);

        AppUser appUser = new AppUser();
        appUser.setUsername(userForm.getUsername());
        appUser.setPassword(bcpe.encode(userForm.getPassword()));
//        appUser.setDirection(directionRepository.findByName(userForm.getDirection()));
//        appUser.setActive(true);
        appUserRepository.save(appUser);
        addRoleToUser2(userForm.getUsername(), userForm.getRoles());
//        addRoleToUser(username, "ADMIN");

        return appUser;

    }

    @Override
    public AppUser editUser(UserForm userForm) {

        AppUser user = appUserRepository.findById(new Long(userForm.getId())).get();

        if (!userForm.getPassword().equals(userForm.getConfirmedPassword())) {
            throw new RuntimeException("Please confirm your password");
        }
        System.out.println("addUser(UserForm userForm)");
        System.out.println(userForm);

        AppUser appUser = new AppUser();
        appUser.setId(user.getId());
        appUser.setUsername(userForm.getUsername());
        appUser.setPassword(bcpe.encode(userForm.getPassword()));
//        appUser.setDirection(directionRepository.findByName(userForm.getDirection()));
//        appUser.setActive(true);

        appUserRepository.save(appUser);
        addRoleToUser2(userForm.getUsername(), userForm.getRoles());

        System.out.println("user edited");
        System.out.println(user);
        return user;
    }
}