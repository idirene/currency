/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dz.idirene.currency.services.admin;


import dz.idirene.currency.model.admin.AppRole;
import dz.idirene.currency.model.admin.AppUser;
import dz.idirene.currency.model.admin.UserForm;


public interface AccountService {
    public AppUser findUserByUsername(String username);

    public AppUser saveUser(String username, String password, String confirmedPassword);
    public AppRole saveRole(AppRole role);
    public AppUser editUser(UserForm user);
    public AppUser loadUserByUsername(String username);
    public void addRoleToUser(String username, String rolename);
    public AppUser addUser( UserForm userForm);
    public long countUser( );

}
