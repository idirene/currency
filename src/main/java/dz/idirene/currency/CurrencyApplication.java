package dz.idirene.currency;

import dz.idirene.currency.model.admin.AppRole;
import dz.idirene.currency.services.admin.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.stream.Stream;

@SpringBootApplication
public class CurrencyApplication {
@Autowired
AccountService accountService;


	@Bean
	BCryptPasswordEncoder getBCPE() {
		return new BCryptPasswordEncoder();
	}
	public static void main(String[] args) {
		SpringApplication.run(CurrencyApplication.class, args);
	}

@Bean
CommandLineRunner start( ) {
			return args -> {
				if (accountService.countUser() == 0) {
					accountService.saveRole(new AppRole(null, "USER"));
					accountService.saveRole(new AppRole(null, "HELPDESK"));
					accountService.saveRole(new AppRole(null, "ADMIN"));
//            Stream.of("user1", "user2", "user3","admin")
					Stream.of("admin")
							.forEach(username -> {
								accountService.saveUser(username, "1234", "1234");
							});

					accountService.addRoleToUser("admin", "ADMIN");
				}


			} ;






		}

}

