package dz.idirene.currency.payload;

import lombok.Data;

import javax.servlet.http.Cookie;

/**
 * Created by rajeevkumarsingh on 19/08/17.
 */
@Data
public class JwtAuthenticationResponse {
    private String accessToken;
    private Cookie cookie;
    private String tokenType = "Bearer";

    public JwtAuthenticationResponse(String accessToken) {
        this.accessToken = accessToken;
    } public JwtAuthenticationResponse( ) {
    }


}
