/*
package dz.idirene.currency.controlleur;



import dz.idirene.currency.exception.AppException;
import dz.idirene.currency.model.admin.Role;
import dz.idirene.currency.model.admin.RoleName;
import dz.idirene.currency.model.admin.User;
import dz.idirene.currency.payload.ApiResponse;
import dz.idirene.currency.payload.JwtAuthenticationResponse;
import dz.idirene.currency.payload.LoginRequest;
import dz.idirene.currency.payload.SignUpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

*/
/**
 * Created by rajeevkumarsingh on 02/08/17.
 *//*

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;


    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;
    @Autowired
    CustomUserDetailsService customUserDetailsService;

    @PostConstruct
    public void init (){


//        Role role = new Role();
//        Role role1 = new Role();
//        role.setName(RoleName.ROLE_USER);
//        role1.setName(RoleName.ROLE_ADMIN);
//        List<Role> list = new ArrayList();
//        list.add(role);
//        list.add(role1);
//        roleRepository.saveAll(list);
//
//        User user = new User("allaoua", "allaoua",
//                "allaoua@gmail.com", passwordEncoder.encode("allaoua"));
//
////        user.setPassword(passwordEncoder.encode(user.getPassword()));
//
//
//        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
//                .orElseThrow(() -> new AppException("User Role not set."));
//
//        user.setRoles(Collections.singleton(userRole));
//
//        User result = userRepository.save(user);
    }

    @ResponseBody
    @PostMapping("/signin")
    public ResponseEntity authenticateUser(@RequestBody @Valid LoginRequest loginRequest, HttpServletResponse response) {
        List <String> roles= customUserDetailsService.loadUserRoles( loginRequest.getUsernameOrEmail());
        List<GrantedAuthority> simpleGrantedAuthorities =roles
                .stream()
                .map(role ->new SimpleGrantedAuthority(role) )
                .collect(Collectors.toList());


        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        Cookie cookie = new Cookie("youcef.header" ,jwt);
        cookie.setPath("/");
        cookie.setDomain("localhost");
        cookie.setMaxAge(60*60);
        response.addCookie(cookie);
        JwtAuthenticationResponse jwtAuthenticationResponse = new JwtAuthenticationResponse();
        jwtAuthenticationResponse.setAccessToken(jwt);
        System.out.println( "cookie value "+cookie.getValue());
        jwtAuthenticationResponse.setCookie(cookie);

        return ResponseEntity.ok(jwtAuthenticationResponse);
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@RequestBody @Valid SignUpRequest signUpRequest) {
        if(userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        // Creating user's account
        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
                signUpRequest.getEmail(), signUpRequest.getPassword());

        user.setPassword(passwordEncoder.encode(user.getPassword()));
//        signUpRequest.

        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new AppException("User Role not set."));

        user.setRoles(Collections.singleton(userRole));

        User result = userRepository.save(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{username}")
                .buildAndExpand(result.getUsername()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
    }
}
*/
