package dz.idirene.currency.controlleur;

import dz.idirene.currency.controlleur.resources.RestConstants;
import dz.idirene.currency.model.Currency;
import dz.idirene.currency.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(RestConstants.currency)
@CrossOrigin
public class CurrencyControlleur {

    CurrencyRepository currencyRepository;

    @Autowired
    public CurrencyControlleur(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    //Find ALL
   
    @GetMapping(path="/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Page<Currency> getAllCurrency (Pageable pageable) {

        Page<Currency> currencyPage = currencyRepository.findAll(pageable);

        return currencyPage;
    }

    // Find By ID
    @RequestMapping(path = "/{currencyId}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity getCurrencyById(
            @PathVariable String currencyId) {
        return new ResponseEntity<>(currencyRepository.findById(currencyId), HttpStatus.OK);
    }

    //ADD
    @RequestMapping(path="add", method = RequestMethod.POST,
    produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
    consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<Currency> createTypeSanction(
    @RequestBody Currency currency
    ){
        currencyRepository.save(currency);
        return new ResponseEntity<>(currency, HttpStatus.CREATED);
    }

    //UPDATE
    @RequestMapping(path="edit", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Currency> updatecCurrency(
            @RequestBody Currency currency
    ){
        currencyRepository.save(currency);
        return new ResponseEntity<>(currency, HttpStatus.CREATED);
    }

    //DELETE
    @RequestMapping(path = "/{currencyId}",
            method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteCurrency(
            @PathVariable("currencyId") String currencyId) {
        System.out.println("TYPE SANCTION ID:" + currencyId);
            currencyRepository.deleteById(currencyId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
